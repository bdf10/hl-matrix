\documentclass{beamer}

\usepackage{nicematrix}
\usepackage{xcolor}
\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{overlay-beamer-styles}

\definecolor{blu}{rgb}{0.2,0.2,0.7}
\definecolor{grn}{rgb}{0.0,0.5,0.0}

\tikzstyle{hl}=[
, rounded corners
, very thick
, fill opacity=0.45
]

\newcommand<>{\hlRow}[2][blu]{
  \tikz
  \pgfmathtruncatemacro{\lastCol}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\nextRow}{#2+1}
  \draw[hl, fill=#1, visible on=#3]
  (row-#2-|col-1) rectangle (row-\nextRow-|col-\lastCol);
}
\newcommand<>{\hlCol}[2][blu]{
  \tikz
  \pgfmathtruncatemacro{\lastRow}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\nextCol}{#2+1}
  \draw[hl, fill=#1, visible on=#3]
  (row-1-|col-#2) rectangle (row-\lastRow-|col-\nextCol);
}
\newcommand<>{\hlMat}[1][blu]{
  \tikz
  \pgfmathtruncatemacro{\lastRow}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\lastCol}{\arabic{jCol}+1}
  \draw[hl, fill=#1, visible on=#2]
  (row-1-|col-1) rectangle (row-\lastRow-|col-\lastCol);
}
\newcommand<>{\hlEntry}[3][blu]{
  \tikz
  \pgfmathtruncatemacro{\nextRow}{#2+1}
  \pgfmathtruncatemacro{\nextCol}{#3+1}
  \draw[hl, fill=#1, visible on=#4]
  (row-#2-|col-#3) rectangle (row-\nextRow-|col-\nextCol);
}
\newcommand<>{\hlDiag}[1][blu]{
  \tikz
  \pgfmathtruncatemacro{\n}{min(\arabic{jCol}, \arabic{iRow})+1}
  \draw[hl, fill=#1, visible on=#2]
  (row-1-|col-1)
  \foreach \i in {2,...,\n}{ |- (row-\i-|col-\i) }
  \foreach \i in {\n,...,2}{ |- (row-\i-|col-\i) }
  -- (row-1-|col-2)
  -- cycle
  ;
}
\newcommand<>{\hlUpper}[1][blu]{
  \tikz
  \pgfmathtruncatemacro{\m}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\n}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\k}{min(\m, \n)}
  \draw[hl, fill=#1, visible on=#2]
  (row-1-|col-1)
  \foreach \i in {2,...,\k}{ |- (row-\i-|col-\i) }
  -- (row-\k-|col-\n)
  -- (row-1-|col-\n)
  -- cycle
  ;
}
\newcommand<>{\hlLower}[1][blu]{
  \tikz
  \pgfmathtruncatemacro{\m}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\n}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\k}{min(\m, \n}
  \draw[hl, fill=#1, visible on=#2]
  (row-1-|col-1)
  \foreach \i in {2,...,\k}{ -| (row-\i-|col-\i) }
  -- (row-\m-|col-\k)
  -- (row-\m-|col-1)
  -- cycle
  ;
}
\newcommand<>{\hlPortion}[5][blu]{
  \tikz
  \pgfmathtruncatemacro{\nextRow}{#4+1}
  \pgfmathtruncatemacro{\nextCol}{#5+1}
  \draw[hl, fill=#1, visible on=#6]
  (row-#2-|col-#3) rectangle (row-\nextRow-|col-\nextCol);
}

\title{Decorating Matrices Using Nicematrix and TikZ}
\author{Brian D.\ Fitzparick}

\begin{document}

\maketitle
\begin{frame}\tableofcontents\end{frame}

\section{Highlighting Matrices}
\subsection{Rows and Columns}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlRow} and \texttt{\textbackslash hlCol}
    highlights rows and columns.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlRow<2>{1} % <2> is the frame #, {1} is the row #
          \hlRow<3>{2}
          \hlRow<4>{3}
          \hlCol<5>[grn]{1} % <5> is the frame #, {1} is the col #, [grn] is the color
          \hlCol<6>[grn]{2}
          \hlCol<7>[red]{3}
          \hlCol<8>[red]{4}
        }
        ]
        1 & 2   & 3  & 4 \\
        5 & 6   & 7  & 8 \\
        9 & -11 & 15 & 107
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}

\subsection{Entries}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlEntry} highlights individual entries.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlEntry<2->{1}{1} % <2-> is the frame #, {1}{1} means "(1,1) entry"
          \hlEntry<3->[grn]{2}{3} % <3-> is the frame #, {2}{3} means "(2,3) entry", [grn] is the color
          \hlEntry<4->[red]{3}{5} % <4-> is the frame #, {1}{1} means "(3,5) entry", [red] is the color
        }
        ]
        1 & -7 & 0 &  5 & 0 \\
        0 &  0 & 1 & -3 & 0 \\
        0 &  0 & 0 &  0 & 1
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}


\subsection{The Diagonal}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlDiag} highlights the diagonal.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlDiag<2->[red] % <2-> is the frame #, [red] is the color
        }
        ]
        7 &  0 & 0 &  0 \\
        0 & -5 & 0 &  0 \\
        0 &  0 & 4 &  0 \\
        0 &  0 & 0 & 27
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}

\subsection{Upper Triangular Matrices}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlUpper} highlights the upper-portion of the matrix.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlUpper<2->[grn] % <2-> is the frame #, [grn] is the color
        }
        ]
        3 &  0 & 5 & 11 \\
        0 & 19 & 4 & -2 \\
        0 &  0 & 6 &  3 \\
        0 &  0 & 0 & 19
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}


\subsection{Lower Triangular Matrices}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlLower} highlights the upper-portion of the matrix.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlLower<2->[red] % <2-> is the frame #, [grn] is the color
        }
        ]
        5 & 0 & 0 & 0 & 0 \\
        4 & 7 & 0 & 0 & 0 \\
        6 & 5 & 4 & 0 & 0 \\
        7 & 9 & 8 & 5 & 0 \\
        3 & 0 & 5 & 8 & 1
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}

\subsection{Entire Matrices}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlMat} highlights the entire matrix.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlMat<2-> % <2-> is the frame #, (blu is the default color)
        }
        ]
        {} -9 &  3 &   0 & -1 & 43 \\
        {}  2 & -1 &  -1 &  0 &  0 \\
        {}  0 &  0 & -40 & -1 & -2 \\
        {}  1 & -1 &   1 &  3 &  2
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}

\subsection{Portions}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Using \texttt{\textbackslash hlPortion} highlights part of the matrix.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlPortion<2>{2}{3}{3}{5} % <2> is the frame #, (blu is the default color), {2}{3} is the nw corner, {3}{5} is the se corner
          \hlPortion<3>[grn]{2}{1}{4}{2} % <3> is the frame #, [grn] is the color, {2}{1} is the nw corner, {4}{2} is the se corner
        }
        ]
        {} -9 &  3 &   0 & -1 & 43 \\
        {}  2 & -1 &  -1 &  0 &  0 \\
        {}  0 &  0 & -40 & -1 & -2 \\
        {}  1 & -1 &   1 &  3 &  2
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}

\section{Annotations}
\subsection{Example}
\begin{frame}{\secname}{\subsecname}

  \begin{example}
    We can also use TikZ to annotate.
    \[
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlMat<2->
        }
        , code-after = {
          \begin{tikzpicture}
            \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
            ($ (row-1-|col-1)!0.5!(row-1-|col-6) $) |- ++(-2mm, 2.5mm)
            node[left] {this is a matrix};
          \end{tikzpicture}
        }
        ]
        {}  1 & -1 & -1 &  1 &  0 \\
        {} -3 & -1 &  0 & -2 &  0 \\
        {}  2 &  0 &  6 & 16 &  2 \\
        {}  3 &  1 &  1 &  1 & -1
      \end{bNiceMatrix}
    \]
  \end{example}

\end{frame}


\end{document}
